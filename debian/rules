#!/usr/bin/make -f
include /usr/share/dpkg/pkg-info.mk
DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
export DEB_HOST_MULTIARCH
py3default = $(subst .,,$(shell py3versions -dv))
HAS_LIBDPDK_DEV=$(shell if [ -f /usr/include/dpdk/rte_version.h ] ; then echo "YES" ;  else echo "NO" ; fi)
DEB_NEON_SIMD_OPTION=$(shell if [ $(DEB_BUILD_ARCH) = armhf ]; then echo "-DNEON_SIMD_ENABLE=OFF" ; else echo "" ; fi)
DEB_MIPSEL_PYTHON_API_OPTION=$(shell if [ $(DEB_BUILD_ARCH) = mipsel ]; then echo "-DENABLE_PYTHON_API=OFF" ; else echo "" ; fi)
DEB_UHD_PACKAGE_CONFIGURATION=$(shell echo "-DCMAKE_BUILD_TYPE=RelWithDebInfo -DLIB_SUFFIX="/$(DEB_HOST_MULTIARCH)" \
			     -DENABLE_RFNOC=True -DENABLE_STATIC_LIBS=OFF \
			     -DUHD_VERSION=$(DEB_VERSION) -DENABLE_PYTHON_API=ON -DENABLE_PYTHON3=ON \
			     -DPYTHON_EXECUTABLE:FILEPATH=/usr/bin/python3 \
			     -DBOOST_PYTHON_COMPONENT=python$(py3default) \
	                     -DPKG_LIB_DIR="/usr/lib/uhd" -DDOXYGEN_HAVE_DOT=True \
			     $(DEB_NEON_SIMD_OPTION) $(DEB_MIPSEL_PYTHON_API_OPTION)")

%:
	dh $@ --sourcedirectory=host --with python3 --with numpy3 --buildsystem=cmake+ninja

override_dh_auto_configure:
	dh_auto_configure -- $(DEB_UHD_PACKAGE_CONFIGURATION)
	if [ $(HAS_LIBDPDK_DEV) = YES ] ; then dh_auto_configure --builddirectory=uhdplain -- \
				$(DEB_UHD_PACKAGE_CONFIGURATION) -DENABLE_DPDK=OFF -DUHD_DPDK_CFLAGS="-O3"; fi

override_dh_auto_build:
	dh_auto_build
	if [ $(HAS_LIBDPDK_DEV) = YES ] ; then dh_auto_build --builddirectory=uhdplain ; fi

# RFNoC test 30 failes if no USRP available
override_dh_auto_test:
	- dh_auto_test

override_dh_auto_install-indep:
	dh_auto_install
	rm -rf debian/tmp/usr/share/man
	rm -rf debian/tmp/usr/share/doc/uhd/LICENSE
	rm -rf debian/tmp/usr/share/doc/uhd/README.md
        # Remove source files used to generate documentation
	rm -rf debian/tmp/usr/share/doc/uhd/doxygen/latex

override_dh_auto_install-arch:
	cp debian/uhd-host.limits debian/uhd-host/etc/security/limits.d/uhd.conf
	cp debian/uhd-host.sysctl debian/uhd-host/usr/lib/sysctl.d/50-uhd-usrp2.conf
	dh_auto_install
	if [ $(HAS_LIBDPDK_DEV) = YES ] ; then \
		dh_auto_install --builddirectory=uhdplain --destdir=debian/tmp/plain ;\
		mkdir -p debian/tmp/dpdk/usr/lib ;\
		mv debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH) debian/tmp/dpdk/usr/lib/$(DEB_HOST_MULTIARCH) ;\
		rm -rf debian/tmp/dpdk/usr/lib/$(DEB_HOST_MULTIARCH)/cmake ;\
		rm debian/tmp/dpdk/usr/lib/$(DEB_HOST_MULTIARCH)/libuhd.so ;\
		rm -rf debian/tmp/dpdk/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig ;\
		mv debian/tmp/plain/usr/lib/$(DEB_HOST_MULTIARCH) debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH) ;\
		mkdir -p debian/tmp/dpdk/usr/bin ;\
		mv debian/tmp/usr/lib/uhd/tests/dpdk_test debian/tmp/dpdk/usr/bin/dpdk_test ;\
		mv debian/tmp/usr/lib/uhd/tests/dpdk_port_test debian/tmp/dpdk/usr/bin/dpdk_port_test ;\
	fi
	chmod -x debian/tmp/usr/lib/uhd/tests/devtest/*.py
	chmod -x debian/tmp/usr/lib/uhd/tests/streaming_performance/parse_benchmark_rate.py
	chmod -x debian/tmp/usr/lib/uhd/tests/streaming_performance/run_benchmark_rate.py
	rm -rf debian/tmp/usr/share/man
	rm -rf debian/tmp/usr/share/doc/uhd/LICENSE
	rm -rf debian/tmp/usr/share/doc/uhd/README.md
        # Remove source files used to generate documentation
	rm -rf debian/tmp/usr/share/doc/uhd/doxygen/latex

override_dh_acc:
	- dh_acc $@
	- cat logs/libuhd-dev/*/log.txt

override_dh_shlibdeps:
	dh_shlibdeps --package=uhd-host --libpackage=libuhd4.1.0
	dh_shlibdeps --package=libuhd4.1.0 --libpackage=libuhd4.1.0
	dh_shlibdeps --package=libuhd4.1.0-dpdk --libpackage=libuhd4.1.0-dpdk
	dh_shlibdeps --package=libuhd4.1.0-dpdk-tests --libpackage=libuhd4.1.0-dpdk
	dh_shlibdeps --package=libuhd-dev --libpackage=libuhd4.1.0
	dh_shlibdeps --package=python3-uhd --libpackage=libuhd4.1.0
